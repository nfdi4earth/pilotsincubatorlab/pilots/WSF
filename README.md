# World Settlement Footprint (WSF): Mapping Global Urbanization

## **Overview**
The **World Settlement Footprint (WSF)** project, spearheaded by the **German Aerospace Center (DLR)** and the **Leibniz Institute of Ecological Urban and Regional Development**, provides detailed global datasets on human settlements to support research and decision-making. With high-resolution data and a focus on urbanization, the WSF suite offers critical insights into land use, population distribution, and environmental change.

### **Contributors**
Led by **Jan-Karl Haug**, **Hendrik Zwenzner**, **Thomas Esch**, and a multidisciplinary team, the project aligns with the FAIR principles, ensuring accessibility, interoperability, and usability for diverse user communities. It was funded under NFDI4Earth.

## **Key Features**
1. **WSF Suite Components**:
   - **WSF 2015 & WSF 2019**: High-resolution (10m) global maps of settlements for 2015 and 2019.
   - **WSF Evolution**: Tracks settlement development annually from 1985 to 2015 at 30m resolution.
   - **WSF 3D**: Provides building height, volume, and area data at 90m resolution.

2. **Data Access and Tools**:
   - Available through platforms like the **EOC EO Products Service** ([STAC Collection View](https://geoservice.dlr.de/eoc/ogc/stac/collections?f=text%2Fhtml)) and standard services like WMS and WCS.
   - Supports seamless integration into workflows via formats like Cloud-Optimized GeoTIFF (COG) and compatibility with tools like QGIS and Jupyter Notebooks.

3. **Interoperability Innovations**:
   - Uses the **SpatioTemporal Asset Catalog (STAC)** for efficient data cataloging and retrieval.
   - Prioritizes FAIR data principles with DOI assignments and long-term archiving.

4. **Practical Applications**:
   - Supports environmental research, disaster risk assessment, urban planning, and socio-economic analyses.
   - Already adopted by organizations like the World Bank and integrated into various global studies.

## **Achievements**
- Released comprehensive datasets for global settlement analysis, including temporal and 3D aspects.
- Developed tools to visualize and process settlement data without extensive downloads, reducing computational burdens.
- Published all data under a **Creative Commons BY 4.0 license**, promoting reuse and modification with proper citation.

## **Challenges and Gaps**
The project achieved its primary goals, but:
- Expanding the 3D data to global coverage posed challenges due to computational demands.
- Further advances, such as automated building type classification using deep learning, require additional resources like GPU processing.

## **Future Directions**
1. **Data Updates**:
   - Ensure consistent updates to datasets, particularly for rapidly transforming regions.
   - Enhance temporal resolution with semi-annual updates for critical areas.

2. **Advanced Analyses**:
   - Incorporate deep-learning techniques for building categorization.
   - Expand applications to assess energy consumption, CO2 emissions, and demographic changes.

3. **Community Engagement**:
   - Develop repositories with comprehensive documentation and tutorials.
   - Foster collaboration with researchers and policymakers to refine services and expand use cases.

The **World Settlement Footprint** project underscores the importance of detailed and accessible settlement data in addressing global challenges. Its evolving datasets and innovative services position it as a cornerstone for environmental and urban research.

For more information, explore the [EOC EO Products Service](https://geoservice.dlr.de/eoc/ogc/stac/collections?f=text%2Fhtml).
